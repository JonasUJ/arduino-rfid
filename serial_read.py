import time
import serial
import serial.tools.list_ports as lp

registered = {}


def findPort():
    print("Looking for a connected Arduino Uno")
    port = False
    while not port:
        ports = [tuple(p) for p in lp.comports()]
        for p in ports:
            if p[1].startswith("Arduino Uno") or p[1].startswith("USB-SERIAL CH340"):
                print(f"Detected {p[1]}")
                port = p[0]
                break
    return port


def connectPort(port):
    return serial.Serial(port=port, timeout=0, baudrate=9600)


def waitUntilRead(arduino):
    data = None
    try:
        while not data:
            data = str(arduino.readline(), 'ASCII')
    except serial.serialutil.SerialException:
        arduino.close()
        print("Disconnect by user")
        return False
    return data

def register(data, name):
    registered[data] = name
    print(f"Registered {name} with id {data}")

def listen(port, arduino):
    print(f"Listening to {port}")

    while True:
        try:
            data = waitUntilRead(arduino)
            if data is False:
                return "TERM"

        
            if data not in registered:
                name = input("New card, name: ")
                register(data, name)
                arduino.flushInput()
            else:
                print(f"Scanned {registered[data]} with id {data}")

        except KeyboardInterrupt:
            arduino.close()
            print(f"Stopped listening to {port}")
            return "KEYINT"

def main():
    while True:
        port = findPort()
        time.sleep(0.1)
        arduino = connectPort(port)
        sig = listen(port, arduino)
        if sig == "KEYINT":
            return

if __name__ == "__main__":
    main()
