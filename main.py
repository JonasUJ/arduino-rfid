# main.py is the main script that controls everything

from pprint import pprint
from datetime import datetime, timedelta
from time import sleep

from src.lectio import getSchedule
from src.port import ComPort, SIG
from src.config import config
from src.schedule import Piece, DayStatus

# Create a Comport with length 14 (the length of card_ids)
port = ComPort(max_length=14, logger=print)

schedules = {}

# Offset is used for testing
# If offset is timedelta(hours=1) it means the
# whole program acts like is was running an hour ago
# If we want to test at 8:00 but the time is 16:30
# we simply set offset to go back 8 hours and 30 minutes
offset = timedelta(hours=0, minutes=0)

# Register downloads a Schedule and associates it with card_id
def register(card_id: str) -> bool:
    try:
        # Get Schedule of the student assigned to 'card_id'
        schedule = getSchedule(config["student_map"][card_id], offset=offset)
    except Exception:
        # If something went wrong we want the LED to turn red
        # Return False for unsuccessful
        return False
    
    # Associate card_id with the downloaded Schedule
    schedules[card_id] = schedule
    print("Downloaded lectio schedule for", schedule.name)
    return True

# Let the user know how to close the program
# They could also just click the x in the corner
print("Press CTRL + C to close")

# We use an infinite loop because we always wan to listen for cards
while True:
    try:
        # Wait until we read a card
        card_id = port.waitUntilRead()
        print(f"Card with id {card_id} was scanned")
    except KeyboardInterrupt:
        # When pressing CTRL + C we break the loop
        break
    
    # Sleep for 0.2 seconds so that the blue led stays on for a little
    sleep(0.2)

    # If the card has not been used before
    if card_id not in schedules:
        # Register the card and record success
        success = register(card_id)

        if not success:
            # If something unexpected happened send FAILURE signal and continue
            print("An error occurred")
            port.send(SIG.FALIURE.value)
            continue
        
        # Send SUCCESS signal if everything went well
        port.send(SIG.SUCCESS.value)
    else:
        port.send(SIG.SUCCESS.value)

    # Get the schedule for the card_id we just read
    schedule = schedules[card_id]

    # Get the current Piece based on the time
    status, piece = schedule.pieceNow()

    # If there is currently no Piece in progress,
    # we do not need to calculate a missing percentage
    if status == DayStatus.AFTER:
        print("The day has ended")
        continue
    elif status == DayStatus.BEFORE:
        print("You are too early")
        continue
    elif status == DayStatus.BREAK:
        print("You have a break")
        continue
    elif status == DayStatus.WEEKEND:
        print("You have a day off")
        continue

    # We let the students be up to 3 minutes late,
    # because we don't expect everyone to scan their card
    # at exaclty when the Piece starts, and simultaneously
    nstart = piece.start + timedelta(minutes=config["max_late"])
    duration = piece.end - nstart

    # Calculate missing percentaage
    percent = (datetime.now() - offset - piece.start) / duration * 100

    # If there is any percentage to assign
    if piece.missing == 100 and percent >= 0:
        piece.missing = percent
    else:
        continue
    
    # Create and send a message the the missing percentage has been calculated
    extra = '\n'.join(f"{k}: {v}" for k, v in piece.extra.items())
    msg = f"""
{schedule.name} skal have {round(percent, 2)}% fravær
fra timen der ligger:
{piece.start} - {piece.end}
{extra}
*Skulle sende til lectio hvis vi havde adgang*
    """
    print(msg)
