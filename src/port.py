# port.py handles connections with a device

from enum import Enum

import serial
import serial.tools.list_ports as lp
from src.config import config


# Signals we can send/recieve to/from the device
class SIG(Enum):

    # Signals are ASCII encoded bytes
    BOOT = bytes(chr(49), 'ASCII')
    SUCCESS = bytes(chr(50), 'ASCII')
    FALIURE = bytes(chr(51), 'ASCII')

# Function that eats 1 parameter and does nothing
def empty_logger(x): return None

# ComPort represents a connection with device
class ComPort:

    def __init__(self, port: str = "", baudrate: int = 9600, max_length: int = -1, logger=empty_logger):

        # Set internal variables based __init__ parameters
        self._baudrate = baudrate
        self._max_length = max_length
        self._auto_detect = True
        self._logger = logger

        # if a port was passed we want to only use that
        # otherwise we can auto-detect one when it's plugged in 
        if port:
            self._auto_detect = False
        else:
            port = self.getPort()

        self._portname = port

        # Wait for connection
        self._port = self._connectPort()

        # Wait for BOOT signal from device
        self._logger("Device booting...")
        while self.waitUntilReadSig() != SIG.BOOT.value:
            pass

        # When we recieve BOOT we know the device is ready
        self._logger("Device has booted")

    # baudrate getter
    @property
    def baudrate(self) -> int:
        # Return the baudrate
        return self._baudrate

    # portname getter
    @property
    def portname(self) -> str:
        # Return the portname
        return self._portname

    # Reconnect to a port if it has been disconnected
    def reconnect(self):
        # Make sure the port is closed properly
        self._port.close()

        # Completely remove the port
        del self._port

        # If we should only use a specific port
        if self._auto_detect:
            self._portname = self.getPort()

        # Wait for connect
        self._port = self._connectPort()
        
        # Wait for BOOT signal
        self._logger("Device booting...")
        while self.waitUntilReadSig() != SIG.BOOT.value:
            pass
        
        # We recieved a BOOT signal
        self._logger("Device has booted")

    # Wait until we have something to read
    def waitUntilRead(self) -> str:
        data = None
        try:
            # We need a loop because we can read 0 bytes
            # We keep trying to read until we get something back
            while not data:
                data = str(self._port.read(self._max_length), 'ASCII')

        except serial.serialutil.SerialException:
            # The device was disconnected from the computer
            # We wait until it is reconnected and try again
            self._logger(f"{self.portname} was disconnected")
            self.reconnect()
            data = self.waitUntilRead()

        # Return the data
        return data

    # Send data to the device
    def send(self, data):
        self._port.write(data)

    # Read a signal from the device
    # Very much like waitUntilRead
    def waitUntilReadSig(self):
        data = None
        try:
            while not data:
                data = bytes(str(self._port.read(), 'ASCII'), 'ASCII')
        except serial.serialutil.SerialException:
            self._logger(f"{self.portname} was disconnected")
            self.reconnect()
            data = self.waitUntilReadSig()
        return data

    # Auto-detect a device when it has been connected
    def getPort(self) -> serial.Serial:
        self._logger("Looking for connected device...")
        port = False

        # We use a loop to keep checking if any of all connected
        # devices is the one we want
        while not port:

            # All available devices we want
            ports = [tuple(p)
                     for p in lp.grep("|".join(config["devices"]))]

            # If there is any ports we want return it
            if len(ports) > 0:
                port = ports[0][0]
                self._logger(f"Detected {ports[0][1]}")
                break
        return port

    # If a port exists
    def _portExists(self, port):
        ports = [tuple(p)[0] for p in lp.comports()]
        return port in ports

    # Connect to self.portname
    def _connectPort(self):
        self._logger(f"Connecting to {self.portname}...")

        # Wait while the port doesn't exist
        # It exists when it has been connected
        while not self._portExists(self.portname):
            pass
        
        # A port was detected, connect
        port = serial.Serial(port=self._portname,
                             timeout=1, baudrate=self._baudrate)

        # Wait until the device is ready
        while not port.isOpen():
            pass

        # Empty serial buffer
        port.read_all()
        
        self._logger(f"{self.portname} finished connecting")
        return port
