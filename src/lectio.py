# lectio.py handles connection to and downloading from the lectio.dk 

import re
from datetime import datetime, timedelta

import requests
from bs4 import BeautifulSoup
from src.config import config
from src.schedule import Piece, Schedule

# The uri we have to visit to see a students schedule
URI_TEMPLATE = "https://www.lectio.dk/lectio/{school_id}/SkemaNy.aspx?type=elev&elevid={elev_id}"

# Pretend we are a normal browser running FireFox and not a Python WebCrawler
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0"
}

# RegularExpressions for extracting the data we are looking for
timestamps_re = re.compile(
    r'(?P<note>[\w\d \(\)\.]*)??\n?(?P<date>\d{1,2}\/\d{1,2}-\d{4}) (?P<start>\d\d:\d\d) til (?P<end>\d\d:\d\d)', flags=re.MULTILINE | re.DOTALL)
extra_attr_re = re.compile(r'\n(\w+): ([\w\d \(\)\.]+)')
name_re = re.compile(r'Eleven (?P<name>[\w ]+),.*')

# Download HTML from uri
def getHTML(uri):
    with requests.get(uri, headers=headers) as resp:
        response = resp.text
    return response

# Get datetime from lectio formatted dates
def getDatetime(date, time) -> datetime:
    day = date.split("/")[0]

    # Days must be of length two
    if len(day) == 1:
        # If day is only one digit, prepend a 0
        day = "0" + day
    
    month, year = date.split("/")[1].split("-")

    # Months must be of length two
    if len(month) == 1:
        # If month is only one digit, prepend a 0
        month = "0" + month

    # Create datetime object and return
    return datetime.strptime(f"{day}/{month}/{year} {time}", "%d/%m/%Y %H:%M")

# Instantiate a Piece based on downloaded information
def getPiece(elem) -> Piece:
    # data is all relevant information for our Piece
    data = elem.get("data-additionalinfo")

    # Find timestamps in data
    times = timestamps_re.search(data)

    # If we cannot find any timestamps, just ignore the Piece
    if not times:
        return

    # Find any extra information that we could use
    extras = extra_attr_re.findall(data)

    # Get start and end of Piece
    start = getDatetime(times.group("date"), times.group("start"))
    end = getDatetime(times.group("date"), times.group("end"))

    # Return the Piece
    return Piece(start=start, end=end, **dict(extras))

# Get a Schedule for the student with elev_id
def getSchedule(elev_id: str, offset=timedelta(0)) -> Schedule:

    # Download relevant HTML and parse
    html = getHTML(URI_TEMPLATE.format(
        school_id=config["school_id"], elev_id=elev_id))
    soup = BeautifulSoup(html, "html.parser")

    # Find all pieces
    pieces = []
    schedule_pieces = soup.find_all(lambda tag: tag.name == "a" and "s2skemabrik" in tag.get("class", "") and "s2bgbox" in tag.get("class", ""))

    # Convert HTML pieces to actual Pieces
    for schedule_piece in schedule_pieces:
        piece = getPiece(schedule_piece)
        if piece:
            pieces.append(piece)
            
    # Find name of student
    name = name_re.match(
        soup.find("div", class_="maintitle").contents[0]).group("name")

    # Create Schedule and return
    return Schedule(name, *pieces, offset=offset)
