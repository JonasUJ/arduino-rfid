# schedule.py handles TimePeriods and Schedules

from enum import Enum
from pprint import pformat
from datetime import datetime, timedelta

# Possible times of day
class DayStatus(Enum):
    BEFORE  = 0
    DURING  = 1
    BREAK   = 2
    AFTER   = 3
    WEEKEND = 4

# A TimePeriod has a start and end
# It represents a period of time
class TimePeriod:

    start = None
    end = None

    # The duration of a TimePeriod is the 
    # time from its start to its end
    def duration(self) -> timedelta:
        return self.end - self.start

    # A TimePeriod starts on the same day as
    # another TimePeriod if their start dates are the same
    def startSameDay(self, other) -> bool:
        return self.start.date() == other.start.date()

    # A TimePeriod is on a day if its start date
    # is equal to that day
    def isOnDay(self, day) -> bool:
        return self.start.date() == day

    # Allow us to compare TimePeriods with the '>' operator
    # A TimePeriod is greater than another if it ends later
    def __gt__(self, other):
        if type(other) is type(self):
            return self.end > other.end
        if type(other) is datetime:
            return self.end > other
        raise NotImplementedError()

    # Allow us to compare TimePeriods with the '<' operator
    # A TimePeriod is less than another if it starts sooner
    def __lt__(self, other):
        if type(other) is type(self):
            return self.end < other.end
        if type(other) is datetime:
            return self.end < other
        raise NotImplementedError()

# A Piece represents the duration of a school subject
# It is a TimePeriod 
class Piece(TimePeriod):

    def __init__(self, start: datetime, end: datetime, **extra):
        self.start = start
        self.end = end

        # 'missing' is how much of the piece the student has missed
        # 100% by default
        self.missing = 100

        self.extra = extra

    # A Piece is at 'time' if 'time' is between its start and end
    def isAt(self, time: datetime) -> bool:
        return self.start < time < self.end

    # A Piece is always True
    def __bool__(self):
        return True

    # A Piece contains a time and date if it is between its start and end
    def __contains__(self, other):
        if type(other) is datetime:
            return self.isAt(other)
        raise NotImplementedError()

    def __repr__(self):
        return f"Piece(start={self.start}, end={self.end}, **{pformat(self.extra)})"

    # Two Pieces are equal if they start at the same time
    def __eq__(self, other):
        if type(other) is type(self):
            return self.start == other.start and self.end == other.end 
        raise NotImplementedError()


# A Schedule represents a list of Pieces
# It is a TimePeriod
class Schedule(TimePeriod):

    def __init__(self, name: str, *pieces, offset=timedelta(0)):
        self.name = name
        self._pieces = list(pieces)

        # We sort the Pieces from earliest to latest
        self._pieces.sort()

        # A Schedule start at the earliest Piece
        self.start = min(self._pieces).start

        # And ends at the latest
        self.end = max(self._pieces).end

        self.offset = offset

    # Return the current Piece based on the current time
    def pieceNow(self) -> (DayStatus, Piece):
        return self.pieceAt(datetime.now() - self.offset)

    # Return the Piece that lays at a specific time
    # and the DayStatus
    def pieceAt(self, time: datetime) -> (DayStatus, Piece):
        next_piece = self._pieces[0]

        # Loop through all Pieces
        for piece in self._pieces:

            # If time is in piece, piece is now
            if time in piece:
                return DayStatus.DURING, piece

            # If piece is closer to time than next_piece, it becomes next_piece
            elif piece.start - time < next_piece.start - time:
                next_piece = piece

        # If there is currently no Piece return the closest
        # and the current DayStatus
        return self.schoolStatus(), next_piece

    # Return a list of todays Pieces
    def today(self):
        date = (datetime.now() - self.offset).date()

        # Get all pieces today in a list
        day = list(filter(lambda p: p.isOnDay(date), self._pieces))

        # Sort the list of pieces
        day.sort()

        return day

    # schoolStatus returns the current DayStatus
    def schoolStatus(self):
        now = datetime.now() - self.offset
        today = self.today()

        # If there are no pieces today assume it's weekend
        if len(today) == 0:
            return DayStatus.WEEKEND

        # The day has yet to start if now is before the earliest piece
        if now < today[0]:
            return DayStatus.BEFORE

        # The day has ended if now is later than the latest piece
        if now > today[-1]:
            return DayStatus.AFTER

        # Loop through all pieces to see if any are now
        for piece in today:
            if now in piece:
                return DayStatus.DURING

        # If neither of the previous things returned
        # we know that we must be in a break between Pieces
        return DayStatus.BREAK

    def __repr__(self):
        return f"Schedule(name={self.name}, *{pformat(self._pieces)})"

    # Get a Piece from the Schedule
    def __getitem__(self, item):
        for piece in self._pieces:
            if item == piece:
                return piece

    # Set a Piece in the Schedule
    def __setitem__(self, item, value):
        for i, piece in enumerate(self._pieces):
            if item == piece:
                self._pieces[i] = item

