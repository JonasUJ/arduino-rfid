#include <SoftwareSerial.h>

// Pins Rx/Tx are 0 and 1
SoftwareSerial SoftSerial(2, 3);

// RGB pins
const int RED = 7;
const int GREEN = 6;
const int BLUE = 5;

// char array for the current card id
char card_id[13];

// How many chars we've read
int count = 0;

// Current char being read
char data;

// Signals
const char BOOT = '1';
const char SUCCESS = '2';
const char FAILURE = '3';

// Response signal
char sig;

// Sets color of the RGB LED
void setColor(int r, int b, int g)
{
    analogWrite(RED, r);
    analogWrite(GREEN, g);
    analogWrite(BLUE, b);
}

void setup()
{
    // Baudrate is 9600
    SoftSerial.begin(9600);
    Serial.begin(9600);

    // Setup RGB
    pinMode(RED, OUTPUT);
    pinMode(GREEN, OUTPUT);
    pinMode(BLUE, OUTPUT);
    setColor(0, 255, 0);

    // Send boot signal
    Serial.print(BOOT);
}

void loop()
{

    // Is a card being scanned
    if (SoftSerial.available())
    {
        // Read from RFID
        // Only reads a single char
        data = SoftSerial.read();

        // Card ids start with a char 2 and end with a char 3
        // Start and stop chars are converted to parentheses
        if (data == 2 || data == 3)
        {
            data += 38;
        }

        // Add char to card_id and increment count
        card_id[count++] = data;

        // Card ids are 14 chars long
        // If count is 14, we've read 1 card id
        if (count == 14)
        {
            // Print to serial port
            Serial.print(card_id);

            // Set color to blue while we wait for a signal from the computer
            setColor(0, 0, 255);
            
            // Reset count so we can read another card
            count = 0;

            // Wait for signal
            while (Serial.available() == 0);

            // Read signal
            sig = Serial.read();

            // Determine success and set relevant color
            switch (sig) {
                case SUCCESS:
                    setColor(0, 255, 0);
                    break;
                case FAILURE:
                    setColor(255, 0, 0);
                    break;
                default:
                    setColor(0, 0, 255);
            }
        }
    }
}
